<?php

namespace DataBundle\Services;

use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;
use Doctrine\ORM\EntityManager;

use DataBundle\Repository\TranslationRepository;
use DataBundle\Entity\Translation;

/**
 */
class TranslationLoader implements LoaderInterface {
	
	/**
	 * @var TranslationRepository
	 */
	private $translationRepo;
	
//	private $languageRepo;


	public function __construct(EntityManager $em) {
	
//		$this->languageRepo = $em->getRepository('DataBundle:Language');
		$this->translationRepo = $em->getRepository('DataBundle:Translation');
	}
	
	public function load($resource, $locale, $domain = 'messages') {
		
        $catalogue = new MessageCatalogue($locale);
		
        $translations = $this->translationRepo->getTranslations($locale, $domain);

        foreach($translations as $translation){
			/* @var $translation Translation */
            $catalogue->set($translation->getLanguageToken(), $translation->getTranslation(), $domain);
        }        
        return $catalogue;
	}
}
