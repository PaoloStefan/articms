<?php

namespace DataBundle\Services;

use Doctrine\ORM\EntityManager;
use DataBundle\Entity\BaseContent;

use DataBundle\Entity\Language;
use DataBundle\Entity\Permission;
use DataBundle\Repository\BaseContentRepository;
use DataBundle\Repository\PermissionRepository;

/**
 *
 */
class Enforcer {
	
	/**
	 *
	 * @var EntityManager
	 */
	private $_em;
	
	/**
	 *
	 * @var BaseContentRepository
	 */
	private $contentRepo;

	/**
	 *
	 * @var PermissionRepository
	 */
	private $permissionRepo;
	
	public function __construct(EntityManager $em, $numero_partecipanti) {
		$this->_em = $em;
		
		$this->contentRepo = $em->getRepository('DataBundle:BaseContent');
		$this->permissionRepo = $em->getRepository('DataBundle:Permission');
	}
	
	/**
	 * 
	 * @param string $role
	 * @param int $contentId
	 * @return Permission
	 * @throws \Exception
	 */
	public function getPermissionFor($role,$contentId) {
		
		/* @var $content BaseContent */
		$content = $this->contentRepo->find($contentId);
		if($content===NULL){
			throw new \Exception("Content not found");
		}
		
		$language = $content->getLanguage();
		$type = $content->getType();
		
		return $this->permissionRepo->findOneBy( compact('role','language','type') );
	}
}
