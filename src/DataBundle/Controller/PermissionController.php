<?php

namespace DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use DataBundle\Entity\Permission;
use DataBundle\Form\PermissionType;

/**
 * Permission controller.
 *
 */
class PermissionController extends Controller
{
    /**
     * Lists all Permission entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $permissions = $em->getRepository('DataBundle:Permission')->findAll();

        return $this->render('permission/index.html.twig', array(
            'permissions' => $permissions,
        ));
    }

    /**
     * Creates a new Permission entity.
     *
     */
    public function newAction(Request $request)
    {
        $permission = new Permission();
        $form = $this->createForm('DataBundle\Form\PermissionType', $permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($permission);
            $em->flush();

            return $this->redirectToRoute('admin_permission_show', array('id' => $permission->getId()));
        }

        return $this->render('permission/new.html.twig', array(
            'permission' => $permission,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Permission entity.
     *
     */
    public function showAction(Permission $permission)
    {
        $deleteForm = $this->createDeleteForm($permission);

        return $this->render('permission/show.html.twig', array(
            'permission' => $permission,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Permission entity.
     *
     */
    public function editAction(Request $request, Permission $permission)
    {
        $deleteForm = $this->createDeleteForm($permission);
        $editForm = $this->createForm('DataBundle\Form\PermissionType', $permission);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($permission);
            $em->flush();

            return $this->redirectToRoute('admin_permission_edit', array('id' => $permission->getId()));
        }

        return $this->render('permission/edit.html.twig', array(
            'permission' => $permission,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Permission entity.
     *
     */
    public function deleteAction(Request $request, Permission $permission)
    {
        $form = $this->createDeleteForm($permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($permission);
            $em->flush();
        }

        return $this->redirectToRoute('admin_permission_index');
    }

    /**
     * Creates a form to delete a Permission entity.
     *
     * @param Permission $permission The Permission entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Permission $permission)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_permission_delete', array('id' => $permission->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
