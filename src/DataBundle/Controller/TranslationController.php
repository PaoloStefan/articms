<?php

namespace DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use DataBundle\Entity\Translation;
use DataBundle\Form\TranslationType;

/**
 * Translation controller.
 *
 */
class TranslationController extends Controller
{
    /**
     * Lists all Translation entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $translations = $em->getRepository('DataBundle:Translation')->findAll();

        return $this->render('translation/index.html.twig', array(
            'translations' => $translations,
        ));
    }

    /**
     * Creates a new Translation entity.
     *
     */
    public function newAction(Request $request)
    {
        $translation = new Translation();
        $form = $this->createForm('DataBundle\Form\TranslationType', $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($translation);
            $em->flush();

            return $this->redirectToRoute('admin_translations_show', array('id' => $translation->getId()));
        }

        return $this->render('translation/new.html.twig', array(
            'translation' => $translation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Translation entity.
     *
     */
    public function showAction(Translation $translation)
    {
        $deleteForm = $this->createDeleteForm($translation);

        return $this->render('translation/show.html.twig', array(
            'translation' => $translation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Translation entity.
     *
     */
    public function editAction(Request $request, Translation $translation)
    {
        $deleteForm = $this->createDeleteForm($translation);
        $editForm = $this->createForm('DataBundle\Form\TranslationType', $translation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($translation);
            $em->flush();

            return $this->redirectToRoute('admin_translations_edit', array('id' => $translation->getId()));
        }

        return $this->render('translation/edit.html.twig', array(
            'translation' => $translation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Translation entity.
     *
     */
    public function deleteAction(Request $request, Translation $translation)
    {
        $form = $this->createDeleteForm($translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($translation);
            $em->flush();
        }

        return $this->redirectToRoute('admin_translations_index');
    }

    /**
     * Creates a form to delete a Translation entity.
     *
     * @param Translation $translation The Translation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Translation $translation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_translations_delete', array('id' => $translation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
