<?php

namespace DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Queste due direttive servono per i campi di tipo Choice ed Entity
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PermissionType extends AbstractType {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
				->add('role', ChoiceType::class, [
					'required' => TRUE,
					'placeholder' => 'Ruolo (obbligatorio)',
					'choices' => [
						'Content editor'=>'ROLE_CONTENT_EDITOR',
						'News editor'=>'ROLE_NEWS_EDITOR',
						'Admin'=>'ROLE_ADMIN',
						],
				])
				->add('language', EntityType::class, [
					'required' => FALSE,
					'label' => 'Lingua',
					'class' => 'DataBundle:Language',
					'choice_label' => 'name',
					'placeholder' => 'Lingua (opzionale)',
					'empty_data' => null,
				])
				->add('type', EntityType::class, [
					'required' => TRUE,
					'label' => 'Tipo di contenuto',
					'class' => 'DataBundle:ContentType',
					'choice_label' => 'name',
					'placeholder' => 'Tipo di contenuto (obbligatorio)',
				])
				->add('canRead',null, ['attr'=>['class'=>'pluto'],
					'label_attr'=>['class'=>'checkbox-inline']
					])
				->add('canCreate')
				->add('canUpdate')
				->add('canDelete')
				->add('canReadOwn')
				->add('canUpdateOwn')
				->add('canDeleteOwn')
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'DataBundle\Entity\Permission'
		));
	}

}
