<?php

namespace DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ContentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('createdOn')
//            ->add('updatedOn')
//            ->add('createdBy')
//            ->add('updatedBy')
            ->add('active')
            ->add('highlight')
            ->add('home')
            ->add('title')
            ->add('subtitle')
//            ->add('slug')
            ->add('body')
            ->add('language', EntityType::class, [
					'required' => FALSE,
					'label' => 'Lingua',
					'class' => 'DataBundle:Language',
					'choice_label' => 'name',
					'placeholder' => 'Lingua (opzionale)',
					'empty_data' => null,
				])
            ->add('type', EntityType::class, [
					'required' => TRUE,
					'label' => 'Tipo di contenuto',
					'class' => 'DataBundle:ContentType',
					'choice_label' => 'name',
					'placeholder' => 'Tipo di contenuto (obbligatorio)',
				])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DataBundle\Entity\Content'
        ));
    }
}
