<?php

namespace DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;

class TranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		
		if(!empty($options) && !empty($options['choices'])){
			$builder
				->add( 'languageIsoCode', ChoiceType::class,[
					'choices'=>$options['choices']
					]);
			
		}else{
		
			$builder
				// Usa le "locale" esistenti, preferendone alcune
				->add( 'languageIsoCode', LocaleType::class,[
					'preferred_choices'=>['en_US','en_GB','it_IT','fr_FR','de_DE','es_ES']
					])
//				// Usa tutte le lingue esistenti al mondo
//				->add( 'languageIsoCode', LocaleType::class,[
//					'preferred_choices'=>['en_US','en_GB','it_IT','fr_FR','de_DE','es_ES']
//					])
			;
		}
		
		$builder
            ->add('domain', null, ['empty_data'=>'messages','required'=>FALSE])
            ->add('languageToken')
            ->add('translation')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DataBundle\Entity\Translation'
        ));
    }
}
