<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * ProductContent
 *
 * @ORM\Entity
 * 
 * @Gedmo\Loggable
 */
class ProductContent extends BaseContent
{
    /**
     * @var Product
     *
 	 * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id",nullable=true)
     */
    private $product;

    /**
     * Set product
     *
     * @param \DataBundle\Entity\Product $product
     *
     * @return ProductContent
     */
    public function setProduct(\DataBundle\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \DataBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
