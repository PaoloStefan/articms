<?php

namespace DataBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class ArticUser extends BaseUser{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	public function __construct()
    {
        parent::__construct();
        // your own logic
    }
	
	/**
	 * @todo migliorami!
	 * @return string
	 */
	public function fullName() {
		return $this->username;
	}
}
