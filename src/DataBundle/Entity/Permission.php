<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="permission",
 *            uniqueConstraints= {
 *              @ORM\UniqueConstraint(name="idx_name",columns={"role","language_id"})
 *            })
 * @ORM\Entity(repositoryClass="DataBundle\Repository\PermissionRepository")
)
 */
class Permission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=64)
     */
    private $role;

    /**
     * @var Language
     *
	 * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id",nullable=true)
     */
    private $language;


    /**
     * @var bool
     *
     * @ORM\Column(name="canRead", type="boolean")
     */
    private $canRead;

    /**
     * @var bool
     *
     * @ORM\Column(name="canCreate", type="boolean")
     */
    private $canCreate;

    /**
     * @var bool
     *
     * @ORM\Column(name="canUpdate", type="boolean")
     */
    private $canUpdate;

    /**
     * @var bool
     *
     * @ORM\Column(name="canDelete", type="boolean")
     */
    private $canDelete;

    /**
     * @var bool
     *
     * @ORM\Column(name="canReadOwn", type="boolean")
     */
    private $canReadOwn;

    /**
     * @var bool
     *
     * @ORM\Column(name="canUpdateOwn", type="boolean")
     */
    private $canUpdateOwn;

    /**
     * @var bool
     *
     * @ORM\Column(name="canDeleteOwn", type="boolean")
     */
    private $canDeleteOwn;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Permission
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set canRead
     *
     * @param boolean $canRead
     *
     * @return Permission
     */
    public function setCanRead($canRead)
    {
        $this->canRead = $canRead;

        return $this;
    }

    /**
     * Get canRead
     *
     * @return bool
     */
    public function getCanRead()
    {
        return $this->canRead;
    }

    /**
     * Set canCreate
     *
     * @param boolean $canCreate
     *
     * @return Permission
     */
    public function setCanCreate($canCreate)
    {
        $this->canCreate = $canCreate;

        return $this;
    }

    /**
     * Get canCreate
     *
     * @return bool
     */
    public function getCanCreate()
    {
        return $this->canCreate;
    }

    /**
     * Set canUpdate
     *
     * @param boolean $canUpdate
     *
     * @return Permission
     */
    public function setCanUpdate($canUpdate)
    {
        $this->canUpdate = $canUpdate;

        return $this;
    }

    /**
     * Get canUpdate
     *
     * @return bool
     */
    public function getCanUpdate()
    {
        return $this->canUpdate;
    }

    /**
     * Set canDelete
     *
     * @param boolean $canDelete
     *
     * @return Permission
     */
    public function setCanDelete($canDelete)
    {
        $this->canDelete = $canDelete;

        return $this;
    }

    /**
     * Get canDelete
     *
     * @return bool
     */
    public function getCanDelete()
    {
        return $this->canDelete;
    }

    /**
     * Set canReadOwn
     *
     * @param boolean $canReadOwn
     *
     * @return Permission
     */
    public function setCanReadOwn($canReadOwn)
    {
        $this->canReadOwn = $canReadOwn;

        return $this;
    }

    /**
     * Get canReadOwn
     *
     * @return bool
     */
    public function getCanReadOwn()
    {
        return $this->canReadOwn;
    }

    /**
     * Set canDeleteOwn
     *
     * @param boolean $canDeleteOwn
     *
     * @return Permission
     */
    public function setCanDeleteOwn($canDeleteOwn)
    {
        $this->canDeleteOwn = $canDeleteOwn;

        return $this;
    }

    /**
     * Get canDeleteOwn
     *
     * @return bool
     */
    public function getCanDeleteOwn()
    {
        return $this->canDeleteOwn;
    }



    /**
     * Set language
     *
     * @param \DataBundle\Entity\Language $language
     *
     * @return Permission
     */
    public function setLanguage(\DataBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \DataBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }


    /**
     * Set canUpdateOwn
     *
     * @param boolean $canUpdateOwn
     *
     * @return Permission
     */
    public function setCanUpdateOwn($canUpdateOwn)
    {
        $this->canUpdateOwn = $canUpdateOwn;

        return $this;
    }

    /**
     * Get canUpdateOwn
     *
     * @return boolean
     */
    public function getCanUpdateOwn()
    {
        return $this->canUpdateOwn;
    }
}
