<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * BaseContent
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\Entity(repositoryClass="DataBundle\Repository\BaseContentRepository")
 * @ORM\DiscriminatorColumn(name="content_type", type="string")
 * @ORM\DiscriminatorMap({"base" = "BaseContent", "product" = "ProductContent"})
 * 
 * @Gedmo\Loggable
 */
class BaseContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetimetz")
	 * @Gedmo\Timestampable(on="create")
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetimetz",nullable=true)
	 * @Gedmo\Timestampable(on="change",field={"title","body"})
     */
    private $updatedOn;

    /**
     * @var ArticUser
     *
 	 * @ORM\ManyToOne(targetEntity="ArticUser")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id",nullable=false)
 	 * @Gedmo\Blameable(on="create")
     */
    private $createdBy;

    /**
     * @var ArticUser
     *
 	 * @ORM\ManyToOne(targetEntity="ArticUser")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id",nullable=true)
	 * @Gedmo\Blameable(on="change",field={"title","body"})
     */
    private $updatedBy;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="highlight", type="boolean")
     */
    private $highlight;

    /**
     * @var bool
     *
     * @ORM\Column(name="home", type="boolean")
     */
    private $home;

    /**
     * @var Language
     *
	 * @ORM\ManyToOne(targetEntity="Language",inversedBy="contents")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id",nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
	 * @Gedmo\Versioned
	 * @Gedmo\Translatable
     */
    private $title;

	/**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255,nullable=true)
	 * @Gedmo\Versioned
     */
    private $subtitle;

    /**
     * @var string
     *	
     * @ORM\Column(name="slug", type="string", length=128)
	 * @Gedmo\Versioned
	 * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

	/**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
	 * @Gedmo\Versioned
     */
    private $body;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return BaseContent
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn
     *
     * @param \DateTime $updatedOn
     *
     * @return BaseContent
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return BaseContent
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set highlight
     *
     * @param boolean $highlight
     *
     * @return BaseContent
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * Get highlight
     *
     * @return boolean
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * Set home
     *
     * @param boolean $home
     *
     * @return BaseContent
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return boolean
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BaseContent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return BaseContent
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return BaseContent
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return BaseContent
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdBy
     *
     * @param \DataBundle\Entity\ArticUser $createdBy
     *
     * @return BaseContent
     */
    public function setCreatedBy(\DataBundle\Entity\ArticUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \DataBundle\Entity\ArticUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \DataBundle\Entity\ArticUser $updatedBy
     *
     * @return BaseContent
     */
    public function setUpdatedBy(\DataBundle\Entity\ArticUser $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \DataBundle\Entity\ArticUser
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set language
     *
     * @param \DataBundle\Entity\Language $language
     *
     * @return BaseContent
     */
    public function setLanguage(\DataBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \DataBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

}
