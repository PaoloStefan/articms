<?php

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Translation
 *
 * @ORM\Table(name="translation",
 *            uniqueConstraints= {
 *              @ORM\UniqueConstraint(name="idx_dictionary",columns={"language","domain","lang_token"})
 *            })
 * @ORM\Entity(repositoryClass="DataBundle\Repository\TranslationRepository")
 */
class Translation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    private $languageIsoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=64)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="lang_token", type="string", length=128)
     */
    private $languageToken;

    /**
     * @var string
     *
     * @ORM\Column(name="translation", type="text")
     */
    private $translation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set languageIsoCode
     *
     * @param string $languageIsoCode
     *
     * @return Translation
     */
    public function setLanguageIsoCode($languageIsoCode)
    {
        $this->languageIsoCode = $languageIsoCode;

        return $this;
    }

    /**
     * Get languageIsoCode
     *
     * @return string
     */
    public function getLanguageIsoCode()
    {
        return $this->languageIsoCode;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Translation
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set languageToken
     *
     * @param string $languageToken
     *
     * @return Translation
     */
    public function setLanguageToken($languageToken)
    {
        $this->languageToken = $languageToken;

        return $this;
    }

    /**
     * Get languageToken
     *
     * @return string
     */
    public function getLanguageToken()
    {
        return $this->languageToken;
    }

    /**
     * Set translation
     *
     * @param string $translation
     *
     * @return Translation
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;

        return $this;	
    }

    /**
     * Get translation
     *
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }
}
