<?php

namespace DataBundle\Repository;

/**
 * TranslationRepository
 *
 */
class TranslationRepository extends \Doctrine\ORM\EntityRepository
{

	public function getTranslations( $languageIsoCode, $domain) {
		return $this->findBy(['languageIsoCode'=>$languageIsoCode, 'domain'=>$domain]);
	}

}
