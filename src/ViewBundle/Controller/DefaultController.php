<?php

namespace ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use DataBundle\Entity\Nation;
use DataBundle\Entity\Language;
use DataBundle\Repository\NationRepository;

use DataBundle\Services\Enforcer;

class DefaultController extends Controller
{
    public function homeAction()
    {
        return $this->render('ViewBundle:Default:home.html.twig');
    }
	
    public function demoAction()
    {
        return $this->render('ViewBundle:Default:demo.html.twig');
    }
	
	public function displayNationAction($iso='IT') {
		
		/* @var $repo NationRepository */
		$repo = $this->getDoctrine()->getRepository("DataBundle:Nation");

		$nation = $repo->findOneBy(['isoCode'=>$iso]);
		
		return $this->render('ViewBundle:Default:nation.html.twig',
				[
					'nation'=>$nation,
					'username'=>'Paolo',
				]);
	}
	
	public function createNationAction($iso='IT',$name='Italia')
    {
		$em = $this->getDoctrine()->getManager();
		$new_nation = new Nation();
		$new_nation->setIsoCode($iso)->setName($name);
		
		$em->persist($new_nation);
		$em->flush();
		
		/* @var $repo NationRepository */
		$repo = $this->getDoctrine()->getRepository("DataBundle:Nation");
		
		$nations = $repo->findAll();
		
		$message= 'Messaggio:<br>';
		foreach ($nations as $nation) {
			$message .= $nation->getIsoCode().'<br>';
		}
		
		$nation_uk = $repo->find(1);
		$nation_uk_array = $repo->findBy(['isoCode'=>'UK']);
		$nation_uk = $repo->findOneBy(['isoCode'=>'UK']);
		
		$message .= $nation->getIsoCode().'<br>';

        return $this->render('ViewBundle:Default:empty.html.twig',['message'=>$message,]);
    }
	
	public function createLangNatAction() {
		
		$em = $this->getDoctrine()->getManager();
		$nation = $this->getDoctrine()->getRepository("DataBundle:Nation")->findOneBy(['isoCode'=>'CH']);
		
		if($nation===NULL){		
			$nation = new Nation();
			$nation->setIsoCode('CH')->setName('Svizzera');
		}

		foreach( $nation->getLanguages() as $lang){
			/* @var $lang Language */
			$lang->setActive(FALSE);
		}
		
//		$lang_it = new Language();
//		$lang_it->setIsoCode('it_ch')
//				->setName('Italiano svizzero')
//				->setActive(TRUE)
//				->setNation($nation)
//				->setSort(1)
//				;
//		
//		$lang_fr = new Language();
//		$lang_fr->setIsoCode('fr_ch')
//				->setName('Francese svizzero')
//				->setActive(TRUE)
//				->setNation($nation)
//				->setSort(2)
//				;

//		$nation->addLanguage($lang_it)->addLanguage($lang_fr);
		
		$em->persist($nation);
		
		$em->flush();
		
//		$nation_2 = $this->getDoctrine()->getRepository("DataBundle:Nation")
//				->findOneBy(['isoCode'=>'CH']);
		
		return $this->render('ViewBundle:Default:nation.html.twig',
				[
					'nation'=>$nation,
					'username'=>'Paolo',
				]);
	}
	
	/**
	 * Controlla i permessi presenti per il tipo di contenuto del contenuto con id `$contentId`
	 * e per il ruolo `$role`.
	 * 
	 * @param Request $request
	 * @param string $role
	 * @param int $contentId
	 * @return type
	 */
	public function enforceAction(Request $request, $role, $contentId ) {
		
		/* @var $enforcer Enforcer */
		$enforcer = $this->get('artic.enforcer');

		
		$permission = $enforcer->getPermissionFor($role, $contentId);
		
		return $this->render('ViewBundle:Default:enforce.html.twig',
				[
					'permission'=>$permission,
					'username'=>'Paolo',
				]);	
	}

	/**
	 * 
	 * @param string $name Nome della Entity da cercare sotto DataBundle
	 * @return type
	 */
	public function debugEntityAction($name) {

		// Queste due righe possono servire se si vogliono i valori dei campi di un record
//		$repo = $this->getDoctrine()->getRepository('DataBundle:'.$name);
//		$entity = $repo->findOneBy([], ['id'=>'ASC']);
		
		$metadata = $this->getDoctrine()->getManager()
			->getClassMetadata('DataBundle\\Entity\\'. $name);
		$fields = [];

		foreach ($metadata->fieldNames as $value) {
			$fields[] = $value;
			// $entity->{'get'.ucfirst($value)}();
		}
		
		return $this->render('ViewBundle:Default:debug-entity.html.twig',
				[
					'page_title'=>'Entity debug',
					'entity_name'=>$name,
					'fields'=>$fields,
					'username'=>'Paolo',
				]);	
	}
}
